<?php

/**
 * @file
 * Drush plugin code for the PWDS Connector module.
 */

/**
 * Implements hook_drush_command().
 */
function pwds_connector_drush_command() {
  $items = array();
  $items['pwds-site-stats'] = array(
    'description' => 'Provides JSON output of site statistics.',
    'engines' => array(
      'outputformat' => array(
        'default' => 'key-value',
        'pipe-format' => 'json',
      ),
    ),
  );
  return $items;
}

/**
 * Drush command execution for 'sitesdb-report'.
 */
function drush_pwds_connector_pwds_site_stats() {
  $data = array();

  $data['theme'] = variable_get('theme_default');
  $data['maintenance_mode'] = variable_get('maintenance_mode') ? 'yes' : 'no';

  // Collect list of all admin UIDs so we can exclude them from other queries
  // without having to re-run this as a subquery for each.
  $admin_uids = db_query("SELECT ur.uid FROM {users_roles} ur JOIN {role} r ON r.rid = ur.rid WHERE r.name = 'administrator'")
    ->fetchCol();

  // If empty, we need to provide a non-empty array to prevent MySQL from erroring.
  if (empty($admin_uids)) {
    $admin_uids = [-1];
  }

  // Get the most recent login time for a non-admin.
  $last_login = db_query_range(
    "SELECT u.access FROM {users} u WHERE u.uid NOT IN (:admin_uids) ORDER BY u.access DESC",
    0,
    1,
    [':admin_uids' => $admin_uids]
  )->fetchField();
  if ($last_login) {
    // Convert from unix timestamp to Y-m-d.
    $tz = new DateTimeZone('America/New_York');
    $date = \DateTime::createFromFormat('U', $last_login, $tz);
    $data['non_admin_last_login'] = $date->format('Y-m-d');
  }
  else {
    $data['non_admin_last_login'] = 0;
  }

  // Total number of non-admin users.
  $users_count = db_query(
    "SELECT COUNT(u.uid) FROM {users} u WHERE u.uid NOT IN (:admin_uids) AND u.uid > 0", [':admin_uids' => $admin_uids]
  )->fetchField();
  $data['non_admin_users_count'] = $users_count;

  // Total number of active (logged in within a year), non-admin users.
  $active_users_count = db_query(
    "SELECT COUNT(u.uid) FROM {users} u WHERE u.uid NOT IN (:admin_uids) AND u.access > UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 year)) ORDER BY u.access DESC", [':admin_uids' => $admin_uids]
  )->fetchField();
  $data['non_admin_active_users_count'] = $active_users_count;

  // Total number of nodes.
  $nodes = db_query('SELECT COUNT(n.nid) FROM {node} n')->fetchField();
  $data['nodes_count'] = $nodes;

  // Total number of nodes, per type.
  $nodes_per_type = db_query('SELECT nt.type, nt.name, COUNT(n.nid) count FROM {node} n JOIN {node_type} nt ON nt.type = n.type GROUP BY nt.type ORDER BY count DESC');
  $data['nodes_per_type'] = array();
  foreach ($nodes_per_type as $record) {
    $data['nodes_per_type'][$record->type] = array(
      'name' => $record->name,
      'count' => $record->count,
    );
  }

  // Total number of terms.
  $terms = db_query('SELECT COUNT(tid) FROM {taxonomy_term_data}')->fetchField();
  $data['terms_count'] = $terms;

  // Total number of terms, per vocabulary.
  $terms_per_vocabulary = db_query('SELECT v.machine_name, v.name, COUNT(t.tid) count FROM {taxonomy_term_data} t JOIN {taxonomy_vocabulary} v ON v.vid = t.vid GROUP BY v.machine_name ORDER BY count DESC');
  $data['terms_per_vocabulary'] = array();
  foreach ($terms_per_vocabulary as $record) {
    $data['terms_per_vocabulary'][$record->machine_name] = array(
      'name' => $record->name,
      'count' => $record->count,
    );
  }

  // Enabled modules (and their versions).
  $data['modules'] = array();
  $result = db_query("SELECT name, info FROM {system} WHERE type = 'module' AND status=1 ORDER BY name");
  foreach ($result as $row) {
    $info = unserialize($row->info);
    $data['modules'][$row->name] = array(
      'name' => $info['name'],
      'version' => $info['version'],
      'project' => $info['project'],
    );
  }

  // Lines of CSS injector.
  $css_injector_lines = 0;
  if (module_exists('css_injector')) {
    $rules = _css_injector_load_rule();
    foreach ($rules as $rule) {
      // Skip disabled rules.
      if (!$rule['enabled']) {
        continue;
      }

      // Check that the rule applies to the current active theme.
      // This is the same logic used in css_injector_init().
      $theme_rules = unserialize($rule['rule_themes']);
      if (!is_array($theme_rules) || empty($theme_rules) || in_array($data['theme'], $theme_rules, TRUE)) {
        // Verify the file for the rule exists, then count its lines.
        $uri = _css_injector_rule_uri($rule['crid']);
        if (file_exists($uri)) {
          $handle = fopen($uri, 'r');
          while (!feof($handle)) {
            fgets($handle);
            $css_injector_lines++;
          }
          fclose($handle);
        }
      }
    }
  }
  $data['css_injector_lines'] = $css_injector_lines;

  return $data;
}
